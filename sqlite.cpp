#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include "sqlite3.h"
#include <cstdlib>
#include <string.h>
#include <iostream>
#include <queue>
#include <algorithm>

using namespace std;
queue <char*> res;

static int callback(void *NotUsed, int argc, char* argv[], char **azColName) {
	int i;
	for (i = 0; i < argc; i++) {
		//printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
		char* copy_argv = (char*)malloc(sizeof(char)*(strlen(argv[i]) + 1));
		copy_argv = strcpy(copy_argv, argv[i]);
		res.push(copy_argv);
	}
	//printf("\n");
	return 0;
}

void find(int i, int k, sqlite3 * db)
{
	char *zErrMsg = 0;
	int rc;
	char sql[] = "SELECT DISTINCT PARENT_ID FROM ID_TAB WHERE ID = ?";
	sql[49] = i + '0';
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	while (k > 1)
	{
		res.push("000");
		while (strcmp(res.front(), "000") != 0)
		{
		not_prev:
			char* a = res.front();
			if (strcmp(a, "000") == 0)
			{
				cout << "doesn't exist" << endl;
				exit(0);
			}
			res.pop();
			if (strcmp(a, "1") == 0)
			{
				goto not_prev;
			}
			sql[49] = a[0];
			rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
		}
		res.pop();
		k--;
	}
	vector <int> result;
	while (!res.empty())
	{
		result.push_back(atoi(res.front()));
		res.pop();
	}
	sort(result.begin(), result.end());
	auto last = unique(result.begin(), result.end());
	result.erase(last, result.end());
	for (auto it : result)
	{
		cout << it << " ";
	}
}


int main(int argc, char* argv[])
{
	sqlite3 *db;
	int rc;
	char *zErrMsg = 0;
	char *sql;
	rc = sqlite3_open("hw3", &db);
	if (rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		exit(0);
	}
	else {
		fprintf(stderr, "Opened database successfully\n");
	}

	sql = "DROP TABLE ID_TAB; ";
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		fprintf(stdout, "Operation done successfully\n");
	}

	sql = "CREATE TABLE ID_TAB("  \
		"ID             INTEGER     NOT NULL," \
		"PARENT_ID      INTEGER );";
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		fprintf(stdout, "Table created successfully\n");
	}

	sql = "DELETE from ID_TAB;"; \
		rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		fprintf(stdout, "Operation done successfully\n");
	}

	sql = "INSERT INTO ID_TAB VALUES(1, NULL);" \
		"INSERT INTO ID_TAB VALUES(2, 1);" \
		"INSERT INTO ID_TAB VALUES(3, 1);" \
		"INSERT INTO ID_TAB VALUES(3, 2);" \
		"INSERT INTO ID_TAB VALUES(4, 2);" \
		"INSERT INTO ID_TAB VALUES(5, 3);" \
		"INSERT INTO ID_TAB VALUES(6, 2);" \
		"INSERT INTO ID_TAB VALUES(6, 3);" \
		"INSERT INTO ID_TAB VALUES(6, 4);" \
		"INSERT INTO ID_TAB VALUES(7, 3);" \
		"INSERT INTO ID_TAB VALUES(7, 4);" \
		"INSERT INTO ID_TAB VALUES(7, 5);" \
		"INSERT INTO ID_TAB VALUES(7, 6);";

	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else {
		fprintf(stdout, "Records created successfully\n");
	}
	int i, k;
	cin >> i >> k;
	find(i, k, db);
	sqlite3_close(db);
	return 0;
}