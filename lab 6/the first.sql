﻿USE [КВН]

 DROP TABLE result;
 DROP TABLE team;
 DROP TABLE game;
 DROP TABLE vuz;
 DROP TABLE city;

CREATE TABLE city (
	city_id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(20),
	);

CREATE TABLE vuz (	
	vuz_id INTEGER NOT NULL,
	CONSTRAINT pk_vuz PRIMARY KEY (vuz_id ASC),
	name VARCHAR(20),
	city_id INTEGER NOT NULL,
	);

ALTER TABLE vuz
	ADD CONSTRAINT fk_vuz FOREIGN KEY (city_id) REFERENCES city(city_id)

CREATE TABLE team (
	team_id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(20),
	vuz_id INTEGER NOT NULL,
	);
	
ALTER TABLE team
	ADD CONSTRAINT fk_team FOREIGN KEY (vuz_id) REFERENCES vuz(vuz_id)	
	
CREATE TABLE game (
	game_id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(20),
	leage VARCHAR(20),
	date DATETIME NOT NULL CONSTRAINT right_date CHECK (date > 1961-08-11),
	place VARCHAR(20)
);

CREATE TABLE result (
	team_id INTEGER NOT NULL
	  REFERENCES team(team_id)
	  ON DELETE CASCADE,
	game_id INTEGER NOT NULL
	  REFERENCES game(game_id)
	  ON DELETE CASCADE,
	rang DECIMAL (6, 2) NOT NULL CONSTRAINT positive_rang CHECK (rang > 0),
	bals DECIMAL (6, 2) NOT NULL CONSTRAINT positive_bals CHECK (bals > 0),
	exit_flag INTEGER NOT NULL CONSTRAINT right_exit_flag CHECK (exit_flag IN(0, 1)),
    CONSTRAINT pk_result PRIMARY KEY (team_id, game_id),
); 

 delete from result;
 delete from team;
 delete from game;
 delete from vuz;
 delete from city;

 INSERT INTO city VALUES (1, 'долгопрудный')
 INSERT INTO city VALUES (2, 'бишкек')
 INSERT INTO city VALUES (3, 'тюмень')
 INSERT INTO city VALUES (4, 'саратов')
 INSERT INTO city VALUES (5, 'москва')
 INSERT INTO city VALUES (6, 'рязань')
 INSERT INTO city VALUES (7, 'урал')
 INSERT INTO city VALUES (8, 'ижевск')
 INSERT INTO city VALUES (9, 'лужки')


 INSERT INTO vuz VALUES (1, 'мфти', 1)
 INSERT INTO vuz VALUES (2, 'кгу', 2)
 INSERT INTO vuz VALUES (3, 'нгу', 3)
 INSERT INTO vuz VALUES (4, 'сгу', 4)
 INSERT INTO vuz VALUES (5, 'мосап', 5)
 INSERT INTO vuz VALUES (6, 'вшэ', 5)
 INSERT INTO vuz VALUES (7, 'рудн', 5)
 INSERT INTO vuz VALUES (8, 'ргрту', 6)
 INSERT INTO vuz VALUES (9, 'мнтгу', 7)
 INSERT INTO vuz VALUES (10, 'хогварц', 8)
 INSERT INTO vuz VALUES (11, 'лгу', 9)
 INSERT INTO vuz VALUES (12, 'нвк', 5)

 INSERT INTO team VALUES (1, 'физтех', 1)
 INSERT INTO team VALUES (2, 'aзия микс', 2)
 INSERT INTO team VALUES (3, 'союз', 3)
 INSERT INTO team VALUES (4, 'саратов', 4)
 INSERT INTO team VALUES (5, 'горизонт', 5)
 INSERT INTO team VALUES (6, 'вышка', 6)
 INSERT INTO team VALUES (7, 'дружба', 7)
 INSERT INTO team VALUES (8, 'привет', 8)
 INSERT INTO team VALUES (9, 'уральские пельмени', 9)
 INSERT INTO team VALUES (10, 'сега мега драйв', 10)
 INSERT INTO team VALUES (11, 'победитель', 11)
 INSERT INTO team VALUES (12, 'не везет', 12)

 INSERT INTO game VALUES(1, '1-ый четверть финал', 'высшая', '01/04/2015', 'москва')
 INSERT INTO game VALUES(2, '2-ой четверть финал', 'высшая', '02/02/2011', 'сочи')
 INSERT INTO game VALUES(3, 'полуфинал', 'первая', '02/03/2012', 'москва')
 INSERT INTO game VALUES(4, 'финал', 'первая', '07/05/2011', 'санкт-петербург')
 INSERT INTO game VALUES(5, 'кубок мэра москвы', 'кубок', '01/10/2010', 'москва')
 INSERT INTO game VALUES(6, 'четверть финал', 'высшая', '13/12/2014', 'москва')
 INSERT INTO game VALUES(7, 'летний кубок', 'кубок', '01/06/2013', 'москва')
 INSERT INTO game VALUES(8, 'летний кубок', 'кубок', '01/06/2013', 'москва')
 INSERT INTO game VALUES(9, 'летний кубок', 'кубок', '01/06/2013', 'москва')
 INSERT INTO game VALUES(10, 'зимний кубок', 'кубок', '31/01/2004', 'долгопрудный')

 INSERT INTO result VALUES(1, 1, 1, 13.5, 1)
 INSERT INTO result VALUES(1, 6, 1, 19.5, 1)
 INSERT INTO result VALUES(2, 1, 1, 13.5, 1)
 INSERT INTO result VALUES(1, 3, 2, 11.0, 1)
 INSERT INTO result VALUES(5, 2, 4, 9.4, 0)
 INSERT INTO result VALUES(2, 3, 1, 15.8, 0)
 INSERT INTO result VALUES(6, 3, 2, 14.4, 1)
 INSERT INTO result VALUES(7, 3, 3, 13.2, 1)
 INSERT INTO result VALUES(6, 4, 5, 10.1, 0)
 INSERT INTO result VALUES(8, 5, 2, 10.2, 0)

 INSERT INTO result VALUES(1, 7, 3, 16.2, 1)
 INSERT INTO result VALUES(2, 7, 2, 17.0, 1)
 INSERT INTO result VALUES(3, 7, 4, 15.4, 0)
 INSERT INTO result VALUES(4, 7, 1, 20.8, 1)
 INSERT INTO result VALUES(5, 7, 7, 11.4, 1)
 INSERT INTO result VALUES(6, 7, 5, 13.2, 1)
 INSERT INTO result VALUES(7, 7, 6, 12.1, 0)
 INSERT INTO result VALUES(1, 8, 4, 20.8, 0)
 INSERT INTO result VALUES(2, 8, 5, 11.4, 0)
 INSERT INTO result VALUES(4, 8, 1, 23.2, 1)
 INSERT INTO result VALUES(5, 8, 2, 22.2, 1)
 INSERT INTO result VALUES(6, 8, 3, 22.1, 1)
 INSERT INTO result VALUES(4, 9, 2, 22.6, 0)
 INSERT INTO result VALUES(5, 9, 7, 23.2, 1)
 INSERT INTO result VALUES(6, 9, 3, 22.1, 0)

 INSERT INTO result VALUES(7, 1, 1, 14.1, 1)
 INSERT INTO result VALUES(1, 10, 2, 13.8, 1)
 INSERT INTO result VALUES(2, 9, 3, 10.4, 1)
 INSERT INTO result VALUES(4, 10, 7, 3.2, 0)
 INSERT INTO result VALUES(5, 10, 8, 2.2, 0)
 INSERT INTO result VALUES(9, 7, 4, 22.1, 0)
 INSERT INTO result VALUES(9, 1, 2, 22.6, 1)
 INSERT INTO result VALUES(9, 5, 5, 23.2, 0)
 INSERT INTO result VALUES(9, 6, 4, 26.1, 0)
 INSERT INTO result VALUES(10, 4, 5, 17.2, 0)
 INSERT INTO result VALUES(10, 5, 3, 19.1, 0)

 INSERT INTO result VALUES(3, 10, 1, 10.1, 1)
 INSERT INTO result VALUES(2, 10, 2, 10.1, 1)
 
 INSERT INTO result VALUES(11, 1, 1, 12.3, 1)
 INSERT INTO result VALUES(11, 6, 1, 14.5, 1)

 INSERT INTO result VALUES(12, 1, 10, 8.2, 0)
 INSERT INTO result VALUES(12, 6, 11, 6.1, 0)
 INSERT INTO result VALUES(12, 3, 12, 7.2, 0)
 INSERT INTO result VALUES(12, 5, 11, 5.1, 0)

SELECT * FROM city
SELECT * FROM vuz
SELECT * FROM team
SELECT * FROM result
SELECT * FROM game;

--Лабораторная 6: Транзакции

/* READ UNCOMMITTED проверка на грязное чтение - не предотвращается*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRANSACTION

SELECT place FROM game WHERE game_id = 1

COMMIT


/* READ COMMITTED проверка на грязное чтение - предотвращается*/
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN TRANSACTION

SELECT place FROM game WHERE game_id = 1

COMMIT

/* READ COMMITTED проверка на потерянные изменения - предотвращается*/
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN TRANSACTION

UPDATE team SET name = 'сеанс_1' where team_id = 1

COMMIT

SELECT * FROM team;

/* READ COMMITTED проверка на неповторяющееся чтение - не предотвращается*/
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN TRANSACTION

SELECT * FROM game where game_id = 2

COMMIT

UPDATE game SET place = 'сочи' where game_id = 2;

/* REPEATABLE READ проверка на неповторяющееся чтение - предотвращается*/
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

BEGIN TRANSACTION

SELECT * FROM game where game_id = 2

COMMIT

UPDATE game SET place = 'сочи' where game_id = 2;

/* REPEATABLE READ фантом - не предотвращается*/
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

BEGIN TRANSACTION

SELECT * FROM game WHERE YEAR(date) >= 2016

COMMIT

DELETE FROM game WHERE game_id = 25

