������� ��������� ���������� ��������� �������:
���������� ����������� ��������:

1) READ UNCOMMITTED �������� �� ������� ������
�����1:
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
�����2:
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
�����1:
BEGIN TRANSACTION
�����2:
BEGIN TRANSACTION
�����1:
SELECT place FROM game WHERE game_id = 1
-- ������ ������.
�����2 :
UPDATE game SET place = '������' WHERE game_id = 1
�����1:
SELECT place FROM game WHERE game_id = 1
-- ������ ������.
�����2 :
ROLLBACK
�����1:
SELECT place FROM game WHERE game_id = 1
--������ ������
COMMIT

�������� ���������� ��� ������ � ����������, ������������� ������� ������
� READ UNCOMMITED �����������.

2) READ COMMITTED �������� �� ������� ������
�����1:
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
�����2:
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
�����1:
BEGIN TRANSACTION
�����2:
BEGIN TRANSACTION
�����1:
SELECT place FROM game WHERE game_id = 1
-- ������ ������.
�����2 :
UPDATE game SET place = '������' WHERE game_id = 1
�����1:
SELECT place FROM game WHERE game_id = 1
-- ���������� ������� �����������
�����2 :
ROLLBACK
�����1:
SELECT place FROM game WHERE game_id = 1
--������ ������
COMMIT

������������� � READ COMMITTED ������� ������ �� �����������. 

3)READ COMMITTED �������� �� ���������� ���������
�����1:
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
�����2:
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
�����1:
BEGIN TRANSACTION
�����2:
BEGIN TRANSACTION
�����1:
UPDATE team SET name = '�����_1' where team_id = 1
ce���2:
UPDATE team SET name = '�����_2' where team_id = 1
-- ���������� ������� �����������
�����1:
COMMIT
SELECT * FROM team - �����������
c����2:
COMMIT
-- � ������ 1 ������� ����� �� ������ SELECT * FROM team
����� ����� �������� �����_2

�������������, ���������� ��������� � READ COMMITTED �� ����������� 

4)READ COMMITTED �������� �� ��������������� ������
�����1:
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
�����2:
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
�����1:
BEGIN TRANSACTION
�����2:
BEGIN TRANSACTION
�����1:
SELECT * FROM game where game_id = 2
-- ����� ����
�����2 :
UPDATE game SET place = 'NewYork' where game_id = 2
COMMIT
c����1 :
SELECT * FROM game where game_id = 2
-- ����� NewYork
COMMIT

�������� ���������� ��� ����������, ����� ������� ����� ��� ���������������
������ � READ COMMITTED �����������

5)REPEATABLE READ �������� �� ��������������� ������
�����1:
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
�����2:
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
�����1:
BEGIN TRANSACTION
�����2:
BEGIN TRANSACTION
�����1:
SELECT * FROM game where game_id = 2
-- ����� ����
�����2 :
UPDATE game SET place = 'NewYork' where game_id = 2
-- ���������� ������� �����������
c����1 :
COMMIT
SELECT * FROM game where game_id = 2
-- ���������� ������� �����������
�����2 :
COMMIT
-- � ������ 1 ������� ����� �� ������ SELECT * FROM game where game_id = 2
����� NewYork

������, � REPEATABLE READ ��������������� ������ �� �����������

6)REPEATABLE READ ������
�����1:
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
�����2:
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
�����1:
BEGIN TRANSACTION
�����2:
BEGIN TRANSACTION
�����1:
SELECT * FROM game WHERE YEAR(date) >= 2016
-- ����� ��� ����� ������� ���
�����2:
INSERT INTO game VALUES(25, '������� ������', '������', '21/03/2016', '������');
COMMIT
�����1:
SELECT * FROM game WHERE YEAR(date) >= 2016
-- �����, ��� ������ �� ������� ������ ���������
COMMIT

�������� ���������� ��� ����������,
������ ������ ����������� � REPEATABLE READ.