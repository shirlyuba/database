USE [���]

/* READ UNCOMMITTED �������� �� ������� ������ - �� �������������*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRANSACTION

UPDATE game SET place = '������' WHERE game_id = 1

ROLLBACK

/* READ COMMITTED �������� �� ������� ������ - �������������*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRANSACTION

UPDATE game SET place = '������' WHERE game_id = 1

ROLLBACK

/* READ COMMITTED �������� �� ���������� ��������� -  �������������*/
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN TRANSACTION

UPDATE team SET name = '�����_2' where team_id = 1

COMMIT


/* READ COMMITTED �������� �� ��������������� ������ - �� �������������*/
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN TRANSACTION

UPDATE game SET place = 'NewYork' where game_id = 2

COMMIT

/* REPEATABLE READ �������� �� ��������������� ������ - �������������*/
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

BEGIN TRANSACTION

UPDATE game SET place = 'NewYork' where game_id = 2

COMMIT

/* REPEATABLE READ ������ - �� ���������������*/
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ

BEGIN TRANSACTION

INSERT INTO game VALUES(25, '������� ������', '������', '21/03/2016', '������');

COMMIT
