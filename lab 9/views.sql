CREATE VIEW game_5year AS
( SELECT game_id
FROM game 
WHERE year(date) <= 2011)


CREATE VIEW game_more5year AS
( SELECT DISTINCT team_id
FROM game JOIN result ON game.game_id = result.game_id
WHERE year(date) > 2011)