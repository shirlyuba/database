
CREATE TRIGGER del_game ON game_5year
INSTEAD OF DELETE  
AS 
DELETE FROM team
WHERE team.team_id NOT IN (SELECT team_id FROM game_more5year)
DELETE FROM game
WHERE game.game_id IN (SELECT game_id FROM game_5year)
DELETE FROM result


CREATE TRIGGER LimitNames ON team 
INSTEAD OF INSERT AS
IF EXISTS (
	SELECT team_id
	FROM team
	WHERE team.name IN (SELECT name FROM inserted)
)
BEGIN
ROLLBACK 
END
ELSE INSERT INTO team SELECT * FROM inserted


CREATE TRIGGER check_flags ON result
AFTER UPDATE AS
IF ((SELECT bals FROM inserted) > 15)
UPDATE result SET exit_flag = 1 WHERE result.bals > 15
ELSE UPDATE result SET exit_flag = 0 WHERE result.bals <= 15