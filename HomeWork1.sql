SELECT model, speed, hd
FROM PC
WHERE price < 500

SELECT DISTINCT maker
FROM Product
WHERE type = 'Printer'

SELECT model, ram, screen
FROM Laptop
WHERE price > 1000

SELECT *
FROM printer
WHERE color  = 'y'

SELECT model, speed, hd
FROM PC
WHERE (price < 600) AND (cd = ('12x') OR (cd = '24x'))

SELECT DISTINCT Product.maker, Laptop.speed 
FROM laptop JOIN product ON product.model = laptop.model 
WHERE laptop.hd >= 10

SELECT PC.model, price
FROM PC join Product on Product.model = Pc.model
WHERE maker = 'B'
UNION
SELECT Laptop.model, price
FROM Laptop join Product on Product.model = Laptop.model
WHERE maker = 'B'
UNION
SELECT Printer.model, price
FROM Printer join Product on Product.model = Printer.model
WHERE maker = 'B'

SELECT DISTINCT maker
FROM Product
WHERE type = 'PC'
EXCEPT 
SELECT DISTINCT maker
FROM Product
WHERE type = 'Laptop'

SELECT DISTINCT maker 
FROM Product join PC on Product.model = PC.model
WHERE speed >= 450

SELECT model, price
FROM Printer
WHERE price = (SELECT MAX(price) FROM Printer)

SELECT AVG(speed) AS AVG_SPEED
FROM PC

SELECT AVG(speed) AS AVG_SPEED
FROM Laptop
WHERE price > 1000

SELECT AVG(speed) AS AVG_SPEED
FROM PC join Product on PC.model = Product.model
WHERE Product.maker = 'A'

SELECT maker, MAX(type) 
FROM product 
GROUP BY maker 
HAVING (COUNT(DISTINCT type) = 1) AND (COUNT(model) > 1)

SELECT hd
FROM PC
GROUP BY hd
HAVING COUNT(PC.hd) >= 2
