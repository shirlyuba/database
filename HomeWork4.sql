28)
SELECT 
CAST (CASE WHEN CAST (SUM(B_VOL) AS NUMERIC(10, 2))
IS NULL THEN 0
ELSE (CAST (SUM(B_VOL) AS NUMERIC(10, 2))) / (SELECT COUNT(Q_ID) FROM utQ) 
END AS NUMERIC (10, 2))
AS avg_paint
FROM utB


29)
SELECT Income_o.point, Income_o.date, inc, out 
FROM Income_o LEFT JOIN Outcome_o ON Income_o.point = Outcome_o.point 
AND Income_o.date = Outcome_o.date 
UNION 
SELECT Outcome_o.point, Outcome_o.date, inc, out 
FROM Income_o RIGHT JOIN Outcome_o ON Income_o.point = Outcome_o.point 
AND Income_o.date = Outcome_o.date


30)
SELECT point, date, SUM(sum_out) AS outcome, SUM(sum_inc) AS income 
FROM( SELECT point, date, SUM(inc) as sum_inc, null AS sum_out 
FROM Income 
GROUP BY point, date 
UNION
SELECT point, date, null as sum_inc, SUM(out) as sum_out 
FROM Outcome 
GROUP BY point, date ) AS record
GROUP BY point, date 
ORDER BY point


31)
SELECT DISTINCT class, country 
FROM Classes 
WHERE (bore >= 16)


32)
SELECT country, CAST(AVG((POWER(bore,3)/2)) AS NUMERIC(6,2)) AS weight 
FROM (SELECT country, Classes.class, bore, name 
FROM Classes LEFT JOIN Ships ON Classes.class = Ships.class 
UNION ALL 
SELECT DISTINCT country, class, bore, ship 
FROM Classes LEFT JOIN Outcomes ON Classes.class = Outcomes.ship 
WHERE ship = class AND ship NOT IN (SELECT name FROM ships) ) as A
WHERE name IS NOT NULL
GROUP BY country


33)
SELECT Outcomes.ship 
FROM Battles LEFT JOIN Outcomes ON Outcomes.battle = Battles.name 
WHERE (Battles.name = 'North Atlantic') AND (Outcomes.result = 'sunk')


34)
SELECT name 
FROM Classes, Ships
WHERE launched >= 1922 AND displacement > 35000 AND type='bb' AND 
Ships.class = Classes.class


35)
SELECT model, type 
FROM Product 
WHERE model NOT LIKE '%[^A-Z]%' OR model NOT LIKE '%[^0-9]%'


36)
SELECT name 
FROM Ships
WHERE class = name 
UNION 
SELECT ship as name 
FROM Classes INNER JOIN Outcomes ON Classes.class = Outcomes.ship


37)
SELECT Classes.class 
FROM Classes LEFT JOIN ( 
SELECT class, name 
FROM Ships 
UNION 
SELECT ship as class, ship as name
FROM Outcomes) AS T ON T.class = Classes.class 
GROUP BY Classes.class 
HAVING COUNT(T.name) = 1


38)
SELECT country 
FROM Classes 
WHERE type = 'bb'
INTERSECT
SELECT country 
FROM Classes 
WHERE type = 'bc'


39)
WITH a AS 
(SELECT out.ship, b.name, b.date, out.result 
FROM outcomes out
LEFT JOIN battles b ON out.battle = b.name ) 
SELECT DISTINCT a.ship FROM a 
WHERE a.ship in
(SELECT ship FROM a c 
WHERE a.date > c.date AND c.result = 'damaged')


40)
SELECT Ships.class, Ships.name, Classes.country 
FROM Ships LEFT JOIN Classes ON Ships.class = Classes.class 
WHERE Classes.numGuns >= 10


41)
SELECT chr, value 
FROM ( SELECT
CAST(model as NVARCHAR(10)) as model,
CAST(speed as NVARCHAR(10)) as speed,
CAST(ram as NVARCHAR(10)) as ram,
CAST(hd as NVARCHAR(10)) as hd,
CAST(cd as NVARCHAR(10)) as cd,
Cast(price as NVARCHAR(10)) as price 
FROM PC 
WHERE code = (SELECT MAX(code) FROM PC) 
) as t 
UNPIVOT 
(value FOR chr IN (model, speed, ram, hd, cd, price) )
 as unpvt


42)
SELECT ship, battle 
FROM Outcomes 
WHERE result = 'sunk'


43)
SELECT name 
FROM Battles 
WHERE YEAR(date) NOT IN 
(SELECT launched 
 FROM Ships 
 WHERE launched IS NOT null)


44)
SELECT name 
FROM Ships
WHERE name LIKE 'R%' 
UNION 
SELECT ship 
From Outcomes 
WHERE ship LIKE 'R%'


45)
SELECT name 
FROM Ships 
WHERE name LIKE '% % %' 
UNION 
SELECT ship
FROM Outcomes 
WHERE ship LIKE '% % %'
