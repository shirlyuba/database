16.
SELECT DISTINCT p1.model, p2.model, p1.speed, p1.ram
FROM PC p1, PC p2
WHERE (p1.speed = p2.speed) AND (p1.ram = p2.ram) AND (p1.model > p2.model)

17.
SELECT DISTINCT type, Laptop.model, speed
FROM Laptop, Product
WHERE (Laptop.speed < ALL (SELECT speed FROM PC)) AND (type = 'Laptop')

18.
SELECT DISTINCT maker, price
FROM Product JOIN Printer ON Product.model = Printer.model 
WHERE color = 'y' AND price = (SELECT min(price) FROM printer WHERE color = 'y')

19.
SELECT DISTINCT maker, AVG(screen)
FROM Product JOIN Laptop ON Product.model = Laptop.model
GROUP BY maker

20.
SELECT maker, COUNT(model)
FROM Product
WHERE type = 'PC'
GROUP BY maker
HAVING COUNT(DISTINCT model) >= 3

21.
SELECT maker, MAX(price)
FROM Product JOIN PC ON Product.model = PC.model
GROUP BY maker

22.
SELECT speed, AVG(price)
FROM PC
GROUP BY speed
HAVING speed > 600

23.
SELECT DISTINCT maker 
FROM Product JOIN PC ON PC.model = Product.model 
WHERE speed >= 750 AND maker IN (
SELECT maker 
FROM Product JOIN Laptop ON Laptop.model = Product.model 
WHERE speed >= 750 )

24.
WITH Inc_Out AS (
 SELECT model, price 
 FROM pc 
 UNION 
 SELECT model, price 
 FROM Laptop 
 UNION 
 SELECT model, price 
 FROM Printer )
SELECT model 
FROM Inc_Out
WHERE price = ( 
SELECT MAX(price) 
FROM  Inc_Out)

25.
SELECT DISTINCT maker
FROM Product
WHERE model IN (
SELECT model
FROM PC
WHERE ram = (
SELECT MIN(ram)
FROM PC )
AND speed = (
SELECT MAX(speed)
FROM PC 
WHERE ram = (
SELECT MIN(ram)
FROM PC ) ) )
AND maker IN (
SELECT maker
FROM Product
WHERE type = 'Printer')

26.
SELECT SUM(Gen.price)/COUNT(Gen.price) FROM (
SELECT price 
FROM PC JOIN Product ON PC.model = Product.model
WHERE Product.maker = 'A' 
UNION all 
SELECT price
FROM Laptop JOIN Product ON Laptop.model = Product.model
WHERE Product.maker = 'A') AS Gen

27.
SELECT maker, AVG(hd)
FROM Product JOIN PC ON Product.model = PC.model
WHERE maker IN (
SELECT maker
FROM Product
WHERE type = 'Printer')
GROUP BY maker
