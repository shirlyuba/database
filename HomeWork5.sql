SELECT Outcomes.ship, displacement, numGuns 
FROM (
SELECT name AS ship, displacement, numGuns 
FROM Ships JOIN Classes 
ON Classes.class = Ships.class 
UNION 
SELECT class AS ship, displacement, numGuns 
FROM Classes) AS s 
RIGHT JOIN Outcomes ON Outcomes.ship = s.ship 
WHERE battle = 'Guadalcanal'


SELECT count(*) OVER (ORDER BY s.maker_count DESC, s.maker, s.model) NO, s.maker, s.model 
FROM (
SELECT count(*) OVER (PARTITION BY maker) maker_count, maker, model 
FROM Product) s


SELECT Classes.class 
FROM Classes LEFT JOIN Ships ON Classes.class = Ships.class 
WHERE CLasses.class IN 
(SELECT ship FROM Outcomes WHERE result = 'sunk') 
OR Ships.name IN 
(SELECT ship FROM Outcomes WHERE result = 'sunk') 
GROUP BY Classes.class


SELECT Ships.name 
FROM Classes JOIN Ships ON Classes.class = Ships.class 
WHERE bore = 16 
UNION 
SELECT Outcomes.ship 
FROM Outcomes JOIN Classes ON Classes.class = Outcomes.ship 
WHERE bore = 16


SELECT DISTINCT battle
FROM Outcomes 
WHERE ship IN (SELECT name  FROM Ships  WHERE class = 'kongo')


WITH CTE AS 
(SELECT Ships.name, Classes.numGuns, Classes.displacement
FROM Ships INNER JOIN Classes ON CLasses.class = Ships.class
UNION
SELECT Outcomes.ship, Classes.numGuns, Classes.displacement
FROM Outcomes INNER JOIN Classes ON CLasses.class = Outcomes.ship)
SELECT s.name
FROM CTE AS s
WHERE s.numGuns >= ALL (SELECT CTE.numGuns FROM CTE WHERE CTE.displacement = s.displacement)


SELECT DISTINCT Ships.name 
FROM Ships JOIN Classes ON Classes.class = Ships.class 
WHERE (UPPER(Classes.country) = 'JAPAN' OR Classes.country IS NULL)
AND (Classes.numGuns >= 9 or Classes.numGuns is NULL) 
AND (Classes.bore < 19 OR Classes.bore IS NULL) 
AND (Classes.displacement <= 65000 OR Classes.displacement IS NULL) 
AND (Classes.type = 'bb' OR Classes.type IS NULL)


SELECT CAST(AVG(CAST(numGuns AS NUMERIC(10, 2))) AS NUMERIC(10, 2))
FROM Classes 
WHERE type = 'bb'


WITH CTE AS 
(SELECT Ships.name, Classes.numGuns
FROM Ships INNER JOIN Classes ON Ships.class = CLasses.class
WHERE Classes.type = 'bb'
UNION
SELECT Outcomes.ship, CLasses.numGuns
FROM Outcomes INNER JOIN Classes ON Outcomes.ship = Classes.class
WHERE CLasses.type = 'bb' )
SELECT CAST(AVG(CAST(numGuns AS NUMERIC(6, 2))) AS NUMERIC(6, 2))
FROM CTE


SELECT CLasses.class, ISNULL(MIN(Ships.launched), NULL)
FROM Classes LEFT JOIN Ships ON Classes.class = Ships.class
GROUP BY CLasses.class


SELECT Classes.class, COUNT(s.ship) 
FROM Classes LEFT JOIN 
( SELECT Outcomes.ship, Ships.class
  FROM Outcomes LEFT JOIN Ships ON Ships.name = Outcomes.ship 
  WHERE Outcomes.result = 'sunk' 
) AS s ON s.class = Classes.class OR s.ship = Classes.class 
GROUP BY Classes.class


WITH CTE AS (SELECT Ships.name, Classes.class
FROM Ships INNER JOIN Classes
ON Ships.class = CLasses.class
UNION
SELECT Outcomes.ship, Classes.class
FROM Outcomes INNER JOIN Classes
ON Outcomes.ship = Classes.class)
SELECT R.class, COUNT(*)
FROM (SELECT class
FROM CTE
GROUP BY class
HAVING COUNT(*) > 2) AS R 
INNER JOIN CTE ON CTE.class = R.class INNER JOIN Outcomes ON CTE.name = Outcomes.ship
WHERE Outcomes.result = 'sunk'
GROUP BY R.class


SELECT a.maker, b.type, 
CAST(CAST((SELECT COUNT(*) 
FROM Product 
WHERE maker = a.maker AND type = b.type) * 100 AS NUMERIC(6,2)) / 
(SELECT COUNT(*) 
FROM Product 
WHERE maker = a.maker) AS NUMERIC(6,2)) AS pr
FROM (SELECT DISTINCT maker FROM Product) AS a, (SELECT DISTINCT type FROM Product) AS b


WITH CTE AS (SELECT ISNULL(Income_o.date, Outcome_o.date) AS date, ISNULL(Income_o.point, Outcome_o.point) AS point, ISNULL(Income_o.inc, 0) - ISNULL(Outcome_o.out,0) AS rest
FROM Income_o FULL JOIN Outcome_o ON Income_o.point = Outcome_o.point AND Income_o.date = Outcome_o.date)
SELECT point, SUM(rest) AS sum_rest
FROM CTE
GROUP BY point


WITH CTE AS (SELECT ISNULL(Income_o.date, Outcome_o.date) AS date, ISNULL(Income_o.point, Outcome_o.point) AS point, ISNULL(Income_o.inc, 0) - ISNULL(Outcome_o.out,0) AS rest
FROM Income_o FULL JOIN Outcome_o ON Income_o.point = Outcome_o.point AND Income_o.date = Outcome_o.date
WHERE ISNULL(Income_o.date, Outcome_o.date) < '20010415')
SELECT point, SUM(rest) AS sum_rest
FROM CTE
GROUP BY point