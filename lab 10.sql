﻿USE [КВН]

 DROP TABLE team;
 DROP TABLE vuz;
 DROP TABLE teamIndex;
 DROP TABLE vuzIndex;
 
CREATE TABLE vuz(	
	vuz_id INTEGER NOT NULL,
	CONSTRAINT pk_vuz PRIMARY KEY (vuz_id ASC),
	name VARCHAR(40)
	);

CREATE TABLE vuzIndex (
	vuz_id INTEGER NOT NULL,
	CONSTRAINT pk_vuzIn PRIMARY KEY (vuz_id ASC),
	name VARCHAR(40)
	);

CREATE TABLE team (
	team_id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(40),
	vuz_id INTEGER NOT NULL,
	count_of_games INTEGER,
	);
	
ALTER TABLE team
	ADD CONSTRAINT fk_team FOREIGN KEY (vuz_id) REFERENCES vuz(vuz_id)	
	
CREATE TABLE teamIndex (
	team_id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR(40),
	vuz_id INTEGER NOT NULL,
	count_of_games INTEGER,
	);

ALTER TABLE teamIndex
	ADD CONSTRAINT fk_teamIn FOREIGN KEY (vuz_id) REFERENCES vuzIndex(vuz_id)


 delete from team;
 delete from vuz;
 delete from teamIndex;
 delete from vuzIndex;


DECLARE @count INT

SELECT @count = 1
WHILE @count <= 100001
  BEGIN
    INSERT INTO vuz(vuz_id, name) VALUES (@count, case ceiling(@count % 8)
		when 0 then 'мфти' + cast(@count AS VARCHAR(40))
		when 1 then 'мгу' + cast(@count AS VARCHAR(40))
		when 2 then 'нгу' + cast(@count AS VARCHAR(40))
		when 3 then 'клм' + cast(@count AS VARCHAR(40))
		when 4 then 'пгу' + cast(@count AS VARCHAR(40))
		when 5 then 'нгу' + cast(@count AS VARCHAR(40))
		when 6 then 'ргрту' + cast(@count AS VARCHAR(40))
		when 7 then 'апу' + cast(@count AS VARCHAR(40))
		end)
    SELECT @count = @count + 1 
	END

SELECT @count = 1
WHILE @count <= 100001
  BEGIN
    INSERT INTO team (team_id, name, vuz_id, count_of_games) VALUES (@count, case ceiling(@count % 10)
		when 0 then 'физтех' + cast(@count AS VARCHAR(40))
		when 1 then 'привет' + cast(@count AS VARCHAR(40))
		when 2 then 'пельмени' + cast(@count AS VARCHAR(40))
		when 3 then 'cега мега' + cast(@count AS VARCHAR(40))
		when 4 then 'пятигорск' + cast(@count AS VARCHAR(40))
		when 5 then 'азия микс' + cast(@count AS VARCHAR(40))
		when 6 then 'вятка' + cast(@count AS VARCHAR(40))
		when 7 then 'кефир' + cast(@count AS VARCHAR(40))
		when 8 then 'союз' + cast(@count AS VARCHAR(40))
		when 9 then 'чп' + cast(@count AS VARCHAR(40))
		end, @count, ROUND(RAND()*100,0))
    SELECT @count = @count + 1
	END

INSERT INTO vuzIndex (vuz_id, name)
SELECT vuz_id, name FROM vuz

INSERT INTO teamIndex (team_id, name, vuz_id, count_of_games)
SELECT team_id, name, vuz_id, count_of_games FROM team


SELECT * FROM team
SELECT * FROM teamIndex
SELECT * FROM vuz
SELECT * FROM vuzIndex

--лабораторная 10: Использование индексов и средств оптимизации запросов
CHECKPOINT
GO
DBCC DROPCLEANBUFFERS
GO

SET STATISTICS TIME ON
SET STATISTICS IO ON


-- пример 1 
SELECT team_id, count_of_games
FROM team
WHERE team_id > 4000 AND count_of_games < 30

/*
(строк обработано: 28314)
Таблица "team". Число просмотров 1, логических чтений 434, физических чтений 0, упреждающих чтений 0, lob логических чтений 0, lob физических чтений 0, lob упреждающих чтений 0.

 Время работы SQL Server:
   Время ЦП = 47 мс, затраченное время = 267 мс.
*/

CREATE NONCLUSTERED INDEX IX_teamIndex ON teamIndex
(
	team_id ASC,
	count_of_games ASC
)

SELECT team_id, count_of_games
FROM teamIndex
WHERE team_id > 4000 AND count_of_games < 30

/*
(строк обработано: 28314)
Таблица "teamIndex". Число просмотров 1, логических чтений 170, физических чтений 0, упреждающих чтений 0, lob логических чтений 0, lob физических чтений 0, lob упреждающих чтений 0.

 Время работы SQL Server:
   Время ЦП = 15 мс, затраченное время = 208 мс.
*/

/* сравнение данных запросов:
запрос 1: стоимость запроса (по отношению к пакету) : 65%

запрос 2: стоимость запроса (по отношению к пакету) : 35%
*/

-- пример 2

SELECT team.name as team_name, vuz.name as vuz_name
FROM vuz JOIN team ON vuz.vuz_id = team.vuz_id
WHERE count_of_games IN (40, 70) AND vuz.name LIKE 'мфти%'

/*
(строк обработано: 226)
Таблица "team". Число просмотров 1, логических чтений 451, физических чтений 0, упреждающих чтений 0, lob логических чтений 0, lob физических чтений 0, lob упреждающих чтений 0.
Таблица "vuz". Число просмотров 1, логических чтений 314, физических чтений 0, упреждающих чтений 0, lob логических чтений 0, lob физических чтений 0, lob упреждающих чтений 0.

 Время работы SQL Server:
   Время ЦП = 63 мс, затраченное время = 68 мс.

   (так как значения в столбце count_of_games рандомные, результаты могут меняться)
*/

CREATE NONCLUSTERED INDEX IX2_teamIndex ON teamIndex
(
	vuz_id ASC,
	count_of_games ASC
)

CREATE NONCLUSTERED INDEX IX_vuzIndex ON vuzIndex
(
	name ASC,
	vuz_id ASC
)

CREATE NONCLUSTERED INDEX IX_vuz_team ON teamIndex(count_of_games) INCLUDE (name, vuz_id)

SELECT teamIndex.name as team_name, vuzIndex.name as vuz_name
FROM vuzIndex JOIN teamIndex ON vuzIndex.vuz_id = teamIndex.vuz_id
WHERE count_of_games IN (40, 70) AND vuzIndex.name LIKE 'мфти%'

/* 
(строк обработано: 226)
Таблица "Worktable". Число просмотров 0, логических чтений 0, физических чтений 0, упреждающих чтений 0, lob логических чтений 0, lob физических чтений 0, lob упреждающих чтений 0.
Таблица "vuzIndex". Число просмотров 1, логических чтений 38, физических чтений 0, упреждающих чтений 0, lob логических чтений 0, lob физических чтений 0, lob упреждающих чтений 0.
Таблица "teamIndex". Число просмотров 2, логических чтений 16, физических чтений 0, упреждающих чтений 0, lob логических чтений 0, lob физических чтений 0, lob упреждающих чтений 0.

 Время работы SQL Server:
   Время ЦП = 16 мс, затраченное время = 13 мс.
*/

/* сравнение данных запросов:
запрос 1: cтоимость запроса (по отношению к пакету): 86%

запрос 2: стоимость запроса (по отношению к пакуте): 14%
*/